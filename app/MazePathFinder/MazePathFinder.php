<?php


namespace App\MazePathFinder;


use Exception;
use SplQueue;

class MazePathFinder
{
    private $rows;
    private $cols;
    private $matrix;
    private $startingPoint = [];
    private $visited = [[]];
    public  $solved = [[]];
    private $rowMoves = [1, -1, 0, 0];
    private $colMoves = [0, 0, 1, -1];
    private $queue;
    private $currentLayer = 1;
    private $nextLayer = 0;
    public $moveCount = 0;
    private $path;


    /**
     * MazePathFinder constructor.
     * @param $maze
     * @throws Exception
     */
    public function __construct($maze)
    {
        $this->queue =  new SplQueue();
        $this->path =  new SplQueue();
        $this->matrix = $maze->matrix;
        $this->rows = $maze->size['rows'];
        $this->cols = $maze->size['cols'];
    }

    /**
     *initialise maze path calculations
     */
    public function init()
    {
        $this->StartingPoint($this->matrix);
        try {
            $this->findShortestPath($this->startingPoint['i'],$this->startingPoint['j']);

        }
        catch(Exception $e) {
            $this->backtraceShortestPath();
            return back()->withError($e->getMessage());
        }
        $this->backtraceShortestPath();
        return;
    }

    /**
     * @param array $matrix
     */
    private function StartingPoint(array $matrix) : void
    {
        for($i = 0; $i < $this->rows; $i++){
            for($j = 0; $j < $this->cols; $j++){
                if($matrix[$i][$j] === 'P'){
                    $this->startingPoint = ['i' => $i, 'j' => $j];
                }
            }
        }
        return;
    }

    /**
     * @param int $i
     * @param int $j
     * @return bool
     */
    private function isValid(int $i, int $j) : bool
    {
       $valid = false;
       if($i < $this->rows
           && $j < $this->cols
           && $i >= 0
           && $j >= 0
           && $this->matrix[$i][$j] !== "M"
           && !isset($this->visited[$i][$j])
       ) {
           $valid = true;
       }

       return $valid;
    }


    /**
     * @param int $startRow
     * @param int $startCol
     * @throws Exception
     */
    private function findShortestPath(int $startRow , int $startCol)
    {
        $end = false;
        $this->queue->enqueue([$startRow, $startCol]);

        while(!$this->queue->isEmpty()){
            $current = $this->queue->dequeue();
            $this->path->enqueue([$current[0], $current[1]]);
            if($this->matrix[$current[0]][$current[1]] === 'E') {
                $end = true;
                break;
            }

            $this->explore($current[0],$current[1]);
            $this->currentLayer--;

            if($this->currentLayer === 0){
                $this->currentLayer = $this->nextLayer;
                $this->nextLayer = 0;
                $this->moveCount++;

            }
        }

        if(!$end) {
            throw new Exception("Maze cannot be solved");
        }

        return;
    }

    /**
     * @param int $row
     * @param int $col
     */
    private function explore(int $row, int $col) : void
    {
        for($i = 0; $i < 4; $i++){
            $nextRow = $row + $this->rowMoves[$i];
            $nextCol = $col + $this->colMoves[$i];
            if($this->isvalid($nextRow, $nextCol)){
                $this->queue->enqueue([$nextRow, $nextCol]);
                $this->path->enqueue([$row, $col, $nextRow, $nextCol]);
                $this->visited[$nextRow][$nextCol] = true;
                $this->nextLayer++;
            }
        }

        return;
    }

    /**
     * @return null
     */
    private function backtraceShortestPath()
    {
        $path = [];
        $current = $this->path->pop();
        while(!$this->path->isEmpty()){
            $previous = $this->path->pop();
            if(!isset($previous[2]) || !isset($previous[3])){
                continue;
            }
            if($current[0] === $previous[2] && $current[1] === $previous[3]){
                array_push($path, [$current[0],$current[1]]);
                $current = $previous;
            }
        }
        for($i = 0; $i < count($path); $i++){
            $this->solved[$path[$i][0]][$path[$i][1]] = 1;
        }
        return;
    }

}
