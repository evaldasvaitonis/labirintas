<?php

namespace App\Http\Controllers;

use App\MapLoader\MapLoader;
use App\MazePathFinder\MazePathFinder;
use Illuminate\Http\Request;

class MazeController extends Controller
{
    public function index($maze, MapLoader $map, Request $request)
    {
        $request->session()->forget('error');
        $maze = $map->getSingleMaze($maze);
        $path = new MazePathFinder($maze);
        $path->init();
        $maze->drawSolvedMaze($path->solved);

        return view('solved', ['title' => 'labirintas', 'maze' => $maze, 'path' => $path]);

    }
}
