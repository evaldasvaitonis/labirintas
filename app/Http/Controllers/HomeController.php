<?php

namespace App\Http\Controllers;

use App\MapLoader\MapLoader;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(MapLoader $mazes)
    {
        return view('homepage', ['title' => 'labirintas', 'mazes' => $mazes->getMazes()]);
    }
}
