<?php


namespace App\MapLoader;

use App\Entity\Maze;
use SimpleXLSX;

class MapLoader
{
    private $dir = 'maps';
    private $mazes = [];

    /**
     * @return array
     */
    public function getMazes() : array
    {
        $foundMaps = $this->getMazeFileNames();

        foreach($foundMaps as $map){
            $data = $this->getMapData($map);
            if($this->isValid($data)){
                $maze = new Maze(basename($map, '.xlsx'), $data);
                array_push($this->mazes, $maze);
            }
        }

        return $this->mazes;
    }

    /**
     * @return array
     */
    public function getMazeFileNames()
    {
        $maps = preg_grep('~\.xlsx$~', scandir($this->dir));
        return $maps;
    }

    /**
     * @param string $fileName
     * @return array
     */
    public function getMapData(string $fileName) : array
    {
        if ($xlsx = SimpleXLSX::parse(public_path() . '/' . $this->dir . '/' . $fileName)) {
            $mapData = $xlsx->rows();
        } else {
            echo SimpleXLSX::parseError();
        }
        if(!isset($mapData)){
            abort(404);
        }

        return $mapData;
    }

    /**
     * @param string $name
     * @return Maze
     */
    public function getSingleMaze(string $name) : Maze
    {
        $data = $this->getMapData($name);
        $maze = new Maze(basename($name, '.xlsx'), $data);
        return $maze;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isValid($data)
    {
        if($this->isExit($data)
           && $this->isStartingPoint($data)
           && $this->isMazeSizeData($data)
           && $this->isMine($data)
        ) {
            return true;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isStartingPoint(array $data) : bool
    {
        $valid = false;
        for($i = 0; $i < count($data); $i++){
            for($j = 0; $j < count($data[$i]); $j++){
                if($data[$i][$j] === 'P'){
                    $valid = true;
                }
            }
        }
        return $valid;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isMine(array $data) : bool
    {
        $valid = false;
        for($i = 0; $i < count($data); $i++){
            for($j = 0; $j < count($data[$i]); $j++){
                if($data[$i][$j] === 'M'){
                    $valid = true;
                }
            }
        }
        return $valid;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isExit(array $data) : bool
    {
        $valid = false;
        for($i = 0; $i < count($data); $i++){
            for($j = 0; $j < count($data[$i]); $j++){
                if($data[$i][$j] === 'E'){
                    $valid = true;
                }
            }
        }
        return $valid;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isMazeSizeData(array $data) : bool
    {
        $valid = false;
        $filtered = array_filter($data[0]);
        if(count($filtered) === 2 && gettype($filtered[0]) === 'integer' && gettype($filtered[1]) === 'integer'){
            $valid = true;
        }

        return $valid;
    }

}
