<?php


namespace App\Entity;



class Maze
{
    public $name;
    public $size;
    public $grid;
    public $matrix;
    public $solvedGrid;

    /**
     * Maze constructor.
     * @param $fileName
     */
    public function __construct($name, $data)
    {
        $this->name = $name;
        $this->matrix = $this->getMatrix($data);
        $this->size = $this->getMazeSize($data);
        $this->grid = $this->drawMaze($this->matrix);
    }

    /**
     * @param array $coordinates
     * @return string
     */
    public function drawMaze(array $coordinates) : string
    {
        $table ='<table class="maze-preview">';
        for($i = 0; $i < count($coordinates); $i++){
            $table .= '<tr>';
            for($j = 0; $j < count($coordinates[$i]); $j++){
                if($coordinates[$i][$j] === 'M'){
                    $table .= "<td class='mine'>" . $coordinates[$i][$j] . '</td>';
                } elseif($coordinates[$i][$j] === 'P') {
                    $table .= "<td class='start'>" . $coordinates[$i][$j] . '</td>';
                } elseif($coordinates[$i][$j] === 'E') {
                    $table .= "<td class='goal'>" . $coordinates[$i][$j] . '</td>';
                } else {
                    $table .= "<td class='empty'>" . $coordinates[$i][$j];
                }

            }
            $table .= '</tr>';
        }
        $table .= '</table>';

        return $table;
    }

    public function drawSolvedMaze(array $solved) : void
    {
        $table ='<table class="maze-solved">';
        for($i = 0; $i < count($this->matrix); $i++){
            $table .= '<tr>';
            for($j = 0; $j < count($this->matrix[$i]); $j++){
                if($this->matrix[$i][$j] === 'M'){
                    $table .= "<td class='mine'>" . $this->matrix[$i][$j] . '</td>';
                } elseif($this->matrix[$i][$j] === 'P') {
                    $table .= "<td class='start'>" . $this->matrix[$i][$j] . '</td>';
                } elseif($this->matrix[$i][$j] === 'E') {
                    $table .= "<td class='goal'>" . $this->matrix[$i][$j] . '</td>';
                } elseif(isset($solved[$i][$j])) {
                    $table .= "<td class='goal' style='background: green'>" . '</td>';
                } else {
                    $table .= "<td class='empty'>" . 'T';
                }
            }
            $table .= '</tr>';
        }
        $table .= '</table>';
        $this->solvedGrid = $table;

        return;
    }

    /**
     * @param array $mazeData
     * @return array
     */
    public function getMazeSize(array $mazeData) : array
    {
        $size = [];
        $filtered = array_filter($mazeData[0]);
        $size['rows'] = $filtered[0];
        $size['cols'] = $filtered[1];

        return $size;
    }

    public function getMatrix($data)
    {
        array_shift($data);
        return $data;
    }
}
