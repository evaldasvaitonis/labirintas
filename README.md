# Petriukas bėga iš minų lauko

**Pradiniai duomenys:**

1. Atsikiruose .XLSX formato dokumentuose yra pateikiami labirintai
2. Kiekvienas labirintas turi X stulpelių ir Y eilučių.  
3. Kiekviename labirinte yra:
      * Unordered sub-list. 
      * Stulpelių skaičius (pažymėta raudona spalva)
      * Eilučių skaičius (pažymėta mėlyna spalva)
      * Petriukas (raidė P)
      * Išėjimas (raidė E)
      * Minos (raidė M)

*Labirinto dokumento pavyzdys:*

![alt text](https://bitbucket.org/rinkodara/labirintas/raw/5f8af8c507841f6a51fbedee159d6ddb325f5c69/lenta.png "")
	
							
							

**Užduotis**

* Pasirinktame labirinte rasti Petriukui greičiausią kelią iki išėjimo. Minas reikia aplenkti. Petriukas gali judėti į viršų, apačią, kairę, dešinę, įstrižai nejuda.

**Programos vartotojo sąsaja. **

* Programa turi nuskaityti visus pagrindinėje direktorijoje esančius .xlsx formato dokumentus ir juos atvaizduoti kaip labirintų pasirinkimą;
* Pasirinkus labirintą, atvaizduoti greičiausią kelią nuo taško P iki E

