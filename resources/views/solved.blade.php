@include('partials.header')
    <div class="row results">
        <div class="col-md-6 text-center">
            @if (session('error'))
                <p class="lead">{{ session('error') }}</p>
            @else
                <p class="lead">Shortest path consists of {{ $path->moveCount }} steps</p>
            @endif

        </div>
    </div>
    <div class="row my-auto d-table wide">
        <div class="big-maze d-table-cell align-middle">
            {!! $maze->solvedGrid  !!}
        </div>
    </div>
@include('partials.footer')