@include('partials.header')
    <div class="row">
        <section class="instructions">
            <p class="lead text-center">Select a maze to solve </p>
            <div class="rules">
                <ul>
                    <li>P - starting point</li>
                    <li>M - mine, it should be avoided</li>
                    <li>E - exit</li>
                </ul>
            </div>
        </section>
    </div>
        @forelse($mazes as $maze)
        <div class="row maze">
            <div class="col-md-6 text-center">
                <table class="table table-bordered">
                    <tr>
                        <td><p class="lead">Name: {{ $maze->name }}</p></td>
                        <td><p class="lead">Size: {{ $maze->size['rows'] }} x {{ $maze->size['cols'] }}</p></td>
                    </tr>
                </table>
                <a class="btn-success btn" href="maze/{{ $maze->name }}.xlsx">Show the shortest path</a>
            </div>
            <div class="col-md-6">
                <p class="lead">Maze map: </p>
                {!! $maze->grid  !!}
            </div>
        </div>
        @empty
            <p>No mazes</p>
        @endforelse


@include('partials.footer')